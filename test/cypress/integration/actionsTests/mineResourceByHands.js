function AssertDepositResourceAboveThatNumber(resourcename, count = 0){      
    cy.resourceDeposit(resourcename).should('be.above', count);
}

context('Test Mining Resourse By Hands', () => {
    beforeEach(() => {
        cy.restartGame();
        cy.startGame(Cypress.env('PlayerBerlin'));
    });

    it('Test Mine Stone', function () {

        //when
        cy.miningSpecificCountResource(Cypress.env("Stone"), 1)        

        //then
        AssertDepositResourceAboveThatNumber(Cypress.env("Stone"))
    });
        
    it('Test Mine Oil', function () {

        //when
        cy.miningSpecificCountResource(Cypress.env("Oil"), 1)

        //then
        AssertDepositResourceAboveThatNumber(Cypress.env("Oil"))
    });

    it('Test Mine IronOre', function () {

        //when
        cy.miningSpecificCountResource(Cypress.env("IronOre"), 1)

        //then
        AssertDepositResourceAboveThatNumber(Cypress.env("IronOre"))
    });

    it('Test Mine CopperOre', function () {

        //when
        cy.miningSpecificCountResource(Cypress.env("CopperOre"), 1)

        //then
        AssertDepositResourceAboveThatNumber(Cypress.env("CopperOre"))        
    });

    it('Test Mine Coal ', function () {

        //when
        cy.miningSpecificCountResource(Cypress.env("Coal"), 1)

        //then
        AssertDepositResourceAboveThatNumber(Cypress.env("Coal"))        
    });  
});