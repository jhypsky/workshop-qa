function AssertOneMineExistAndMiningResourse(resourceName){ 
    cy.findMinesForResource(resourceName).its('length').should('be.equal', 1);   
    cy.resourceDeposit(resourceName).should('be.above', 2);
}

context('Test Build Mine And Production Resource', () => {
    beforeEach(() => {
        cy.restartGame();
        cy.startGame(Cypress.env('PlayerBerlin'));
    });

    it('Test Stone Mine', function () {

        //when
        cy.buildMineOnResource(Cypress.env("Stone"))      

        //then
        AssertOneMineExistAndMiningResourse(Cypress.env("Stone"))   
    });
        
    it('Test Oil Mine', function () {

        //when
        cy.buildMineOnResource(Cypress.env("Oil"))

        //then
        AssertOneMineExistAndMiningResourse(Cypress.env("Oil"))
    });

    it('Test Mine IronOre', function () {

        //when
        cy.buildMineOnResource(Cypress.env("IronOre"))

        //then
        AssertOneMineExistAndMiningResourse(Cypress.env("IronOre"), 2)
    });

    it('Test Mine CopperOre', function () {

        //when
        cy.buildMineOnResource(Cypress.env("CopperOre"))

        //then
        AssertOneMineExistAndMiningResourse(Cypress.env("CopperOre"), 2)        
    });

    it('Test Mine Coal', function () {

        //when
        cy.buildMineOnResource(Cypress.env("Coal"))

        //then
        AssertOneMineExistAndMiningResourse(Cypress.env("Coal"), 2)        
    });   
    
    it('Test Mine Coal', function () {

        //when
        cy.buildMineOnResource(Cypress.env("Coal"))

        //then
        AssertOneMineExistAndMiningResourse(Cypress.env("Coal"), 2)        
    });     
});