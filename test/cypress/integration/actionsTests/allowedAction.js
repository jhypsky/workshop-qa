function AssertAllowedActionForNonResourceCell(){      
    AssertNotAllowMineResourceAndBuildMine();
    AssertAllowBuildAdvanceBuildings()
    AssertCountAllAction(12);
}

function AssertAllowedActionForResourceCell(){      
    AssertAllowMineResourceAndBuildMine();
    AssertAllowBuildAdvanceBuildings();
    AssertCountAllAction(14);
}

function AssertAllowedActionForBuildingCell(){      
    cy.get('#action-DestroyBuilding').should('exist');
    AssertCountAllAction(1);
}


function AssertNotAllowMineResourceAndBuildMine(){
    cy.get('#action-BuildMine').should('not.exist');
    cy.get('#action-MineResource').should('not.exist');    
}

function AssertAllowMineResourceAndBuildMine(){
    cy.get('#action-BuildMine').should('exist');
    cy.get('#action-MineResource').should('exist');    
}

function AssertCountAllAction(expectedCount){
    cy.get('button').its('length').should('be.equal', expectedCount);
}

function AssertAllowBuildAdvanceBuildings(){
    cy.getBuildFactorySelector(Cypress.env('IronPlate')).should('exist');
    cy.getBuildFactorySelector(Cypress.env('Steel')).should('exist');
    cy.getBuildFactorySelector(Cypress.env('SteelCasing')).should('exist');
    cy.getBuildFactorySelector(Cypress.env('RocketShell')).should('exist');
    cy.getBuildFactorySelector(Cypress.env('CopperPlate')).should('exist');
    cy.getBuildFactorySelector(Cypress.env('Wire')).should('exist');
    cy.getBuildFactorySelector(Cypress.env('Circuit')).should('exist');
    cy.getBuildFactorySelector(Cypress.env('Computer')).should('exist');
    cy.getBuildFactorySelector(Cypress.env('Petroleum')).should('exist');
    cy.getBuildFactorySelector(Cypress.env('BasicFuel')).should('exist');
    cy.getBuildFactorySelector(Cypress.env('SolidFuel')).should('exist');
    cy.getBuildFactorySelector(Cypress.env('RocketFuel')).should('exist');
} 


context('Test Allowed Action', () => {
    beforeEach(() => {
        cy.restartGame();
        cy.startGame(Cypress.env('PlayerBerlin'));
    });

    it('Test No Resource Field Action', function () {

        //when
        cy.findResourceOnMap(Cypress.env('None')).click();   

        //then
        AssertAllowedActionForNonResourceCell()        
    });

    it('Test Resource Field Action', function () {

        //when
        cy.findResourceOnMap(Cypress.env('Stone')).click();   

        //then
        AssertAllowedActionForResourceCell()      
    });

    it('Test Building/Mine Field Action', function () {

        //when
        cy.buildMineOnResource(Cypress.env("Stone"))
        cy.findMinesForResource(Cypress.env('Stone')).click();   

        //then
        AssertAllowedActionForBuildingCell()    
    });
});    