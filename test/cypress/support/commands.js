// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This is will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })

Cypress.Commands.add('startGame', (playerName, options = {}) => {
    cy.visit('/');
    cy.get('select').select(playerName);
    cy.get('button').click(); // play as default player
});

Cypress.Commands.add('tickGame', (playerName, options = {}) => {
    cy.wait(1000); // game ticks like that
});

Cypress.Commands.add('stoneDepositText', (options = {}) => {
    return cy.get('#bank-Stone-amount').invoke('text');
});

Cypress.Commands.add('mineResource', (playerName, options = {}) => {
    return cy.get('#action-MineResource').click();
});

Cypress.Commands.add('buildMine', (playerName, options = {}) => {
    return cy.get('#action-BuildMine').click();
});
Cypress.Commands.add('buildFactory', (typeFactory,playerName, options = {}) => {
    return getBuildFactorySelector().click();
});

Cypress.Commands.add('getBuildFactorySelector', (typeFactory, playerName, options = {}) => {
    return cy.get('#action-Build'+ typeFactory + 'Factory');
});

Cypress.Commands.add('findMinesForResource', (typeMines, playerName, options = {}) => {
    return cy.get('td > div > img[alt="Icon of '+ typeMines + 'Mine"]');
});


Cypress.Commands.add('findResourceOnBank', (nameResource, options = {}) => { 
    return cy.get('td > div > img[alt=' + nameResource + ' Icon]:first');
});

Cypress.Commands.add('findResourceOnMap', (nameResource, options = {}) => { 
    return cy.get('td > div > img[alt="Icon of ' + nameResource + '"]:first');
});

Cypress.Commands.add('resourceDeposit', (nameResource, options = {}) => { 
    return cy.get('#bank-'+ nameResource +'-amount').invoke('text');
});

Cypress.Commands.add('buildMineOnResource', (resourceName, options = {}) => { 
    cy.miningSpecificCountResource('Stone',5)     
    cy.findResourceOnMap(resourceName).click();
    cy.tickGame();
    cy.buildMine();    
});


Cypress.Commands.add('miningSpecificCountResource', (resourceName, countResource, options = {}) => {
    for (let i = 0; i < countResource; i++) {
        cy.findResourceOnMap(resourceName).click();
        cy.mineResource();
        cy.tickGame();
    }    
});

Cypress.Commands.add('restartGame', (playerName, options = {}) => {
        cy.request('POST', Cypress.env("serverRestart"));      
});






